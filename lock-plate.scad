// clang-format off
include <utils/utils.scad>;
// clang-format on

plate_size = [ 23.0, 190.0 ];
plate_thickness = 3;
lock_hole_size = [ 11, 60 ];
lock_hole_positions = [
  [ 10, 20 ],
  [ 10, plate_size[1] - 20 - lock_hole_size[1] ],
];
screw_hole_diameter = 8;
screw_hole_positions = [
  [ 16, 7 ],
  [ 10, 95 ],
  [ 16, plate_size[1] - 7 ],
];
debug_holes = false;

/* [Precision] */
$fs = $preview ? 3 : 0.5;
$fa = $preview ? 5 : 1;
epsilon = 0.1 * $fs;

difference()
{
  cube(concat(plate_size, plate_thickness));
  highlight_if(debug_holes) for (pos = lock_hole_positions)
    translate(concat(pos, -epsilon / 2))
      cube(concat(lock_hole_size, plate_thickness + epsilon));
  highlight_if(debug_holes) for (pos = screw_hole_positions)
    translate(concat(pos, -epsilon / 2))
      cylinder(d = screw_hole_diameter, h = plate_thickness + epsilon);
}
